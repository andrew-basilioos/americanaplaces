var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { AppService } from '../../services/app.service';
import { MapLocationPage } from '../map-location/map-location';
var BrandPage = /** @class */ (function () {
    function BrandPage(navCtrl, navParams, appService, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appService = appService;
        this.translate = translate;
        this.current_cities = [];
        this.current_places = [];
        this.please_select_country_ph = "";
        this.please_select_city_ph = "";
        this.please_select_place_ph = "";
    }
    BrandPage.prototype.ngOnInit = function () {
        var _this = this;
        this.translate.get('choosed_keys').subscribe(function (res) {
            _this.choosed_keys = res;
        });
        this.translate.get('country').subscribe(function (res) {
            _this.country_select_options = res.country_select_options;
            _this.please_select_country_ph = res.please_select_country_ph.value;
        });
        this.translate.get('city').subscribe(function (res) {
            _this.city_select_options = res.city_select_options;
            _this.please_select_city_ph = res.please_select_city_ph.value;
        });
        this.translate.get('place').subscribe(function (res) {
            _this.place_select_options = res.place_select_options;
            _this.please_select_place_ph = res.please_select_place_ph.value;
        });
        this.page_data = this.navParams.data;
        this.appService.getCountriesInfo().subscribe(function (data) {
            _this.names = data;
        });
    };
    BrandPage.prototype.collectCities = function () {
        var _this = this;
        this.current_cities = [];
        this.page_data.locations.map(function (location) {
            if (location.loc_country_id == _this.current_country_id) {
                _this.current_cities.push(location);
            }
        });
        this.current_cities = this.removeDuplicates(this.current_cities, 'loc_city_id');
    };
    BrandPage.prototype.removeDuplicates = function (arr, prop) {
        var obj = {};
        for (var i = 0, len = arr.length; i < len; i++) {
            if (!obj[arr[i][prop]])
                obj[arr[i][prop]] = arr[i];
        }
        var newArr = [];
        for (var key in obj)
            newArr.push(obj[key]);
        return newArr;
    };
    BrandPage.prototype.collectPlaces = function () {
        var _this = this;
        this.current_places = [];
        this.page_data.locations.map(function (location) {
            if (location.loc_city_id == _this.current_city.loc_city_id) {
                _this.current_places.push(location);
            }
        });
    };
    BrandPage.prototype.showOnMap = function () {
        this.navCtrl.push(MapLocationPage, { "current_place": this.current_place, "brand_name": this.navParams.data.brand_name });
    };
    BrandPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-brand',
            templateUrl: 'brand.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AppService, TranslateService])
    ], BrandPage);
    return BrandPage;
}());
export { BrandPage };
//# sourceMappingURL=brand.js.map