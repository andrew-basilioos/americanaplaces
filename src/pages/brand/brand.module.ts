import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrandPage } from './brand';
import { CountryName } from '../../pips/country';
import { CityName } from '../../pips/city';

@NgModule({
  declarations: [
    BrandPage,
    CountryName,
    CityName
  ],
  imports: [
    IonicPageModule.forChild(BrandPage),
  ],
})
export class BrandPageModule {}
