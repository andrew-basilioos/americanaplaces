import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TranslateService} from '@ngx-translate/core';

import { AppService } from '../../services/app.service';

import { MapLocationPage } from '../map-location/map-location';

@IonicPage()
@Component({
  selector: 'page-brand',
  templateUrl: 'brand.html',
})
export class BrandPage implements OnInit{
  page_data;
  names;
  current_country_id;
  current_city;
  current_place;
  current_cities = [];
  current_places = [];
  country_select_options;
  city_select_options;
  place_select_options;
  please_select_country_ph = "";
  please_select_city_ph = "";
  please_select_place_ph = "";
  choosed_keys;

  constructor(public navCtrl: NavController, public navParams: NavParams, private appService:AppService, private translate:TranslateService) {}

  ngOnInit() {
    this.translate.get('choosed_keys').subscribe((res)=>{
      this.choosed_keys = res;
    });
    this.translate.get('country').subscribe((res)=>{
      this.country_select_options = res.country_select_options;
      this.please_select_country_ph = res.please_select_country_ph.value;
    });
    this.translate.get('city').subscribe((res)=>{
      this.city_select_options = res.city_select_options;
      this.please_select_city_ph = res.please_select_city_ph.value
    });
    this.translate.get('place').subscribe((res)=>{
      this.place_select_options = res.place_select_options;
      this.please_select_place_ph = res.please_select_place_ph.value;
    });

    this.page_data = this.navParams.data;
    this.appService.getCountriesInfo().subscribe((data)=>{
      this.names = data;
    });

  }

  collectCities(){
    this.current_places = [];
    this.current_place = null;
    this.current_cities = [];
    this.page_data.locations.map((location)=>{
      if(location.loc_country_id == this.current_country_id){
        this.current_cities.push(location);
      }
    });
    this.current_cities = this.removeDuplicates(this.current_cities,'loc_city_id');
  }

  removeDuplicates( arr, prop ) {
    var obj = {};
    for ( var i = 0, len = arr.length; i < len; i++ ){
      if(!obj[arr[i][prop]]) obj[arr[i][prop]] = arr[i];
    }
    var newArr = [];
    for ( var key in obj ) newArr.push(obj[key]);
    return newArr;
  }

  collectPlaces(){    
    this.current_places = [];
    this.current_place = null;
    this.page_data.locations.map((location)=>{
      if(location.loc_city_id == this.current_city.loc_city_id){
        this.current_places.push(location);
      }
    });
  }

  showOnMap(){
    this.navCtrl.push(MapLocationPage,{"current_place":this.current_place,"brand_name":this.navParams.data.brand_name});
  }

}
