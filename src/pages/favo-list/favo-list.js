var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { MapLocationPage } from '../map-location/map-location';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
var FavoListPage = /** @class */ (function () {
    function FavoListPage(navCtrl, navParams, storage, myCam, file) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.myCam = myCam;
        this.file = file;
        this.current_brand_favo_places = [];
    }
    FavoListPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.current_brand_favo_places = [];
        this.current_brand = this.navParams.data;
        this.storage.get(this.current_brand).then(function (value) {
            _this.current_brand_favo_places = value;
        });
    };
    FavoListPage.prototype.checkAttachedUrl = function (i) {
        if (this.current_brand_favo_places[i].attach_url == 'attach_url') {
            return false;
        }
        return true;
    };
    FavoListPage.prototype.goToMap = function (place) {
        this.navCtrl.push(MapLocationPage, { "current_place": place, "brand_name": null });
    };
    FavoListPage.prototype.openCamera = function (i) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.myCam.DestinationType.FILE_URI,
            encodingType: this.myCam.EncodingType.JPEG,
            mediaType: this.myCam.MediaType.PICTURE
        };
        this.myCam.getPicture(options).then(function (imageData) {
            var imageDataName = imageData.replace(/^.*[\\\/]/, '');
            var path = imageData.replace(/[^\/]*$/, '');
            _this.file.moveFile(path, imageDataName, cordova.file.dataDirectory, imageDataName)
                .then(function (data) {
                _this.current_brand_favo_places[i].attach_url = data.nativeURL;
                _this.storage.set(_this.current_brand, _this.current_brand_favo_places);
            });
        }, function (err) {
            console.log(err);
        });
    };
    FavoListPage.prototype.deleteThisPlace = function (i) {
        var _this = this;
        if (this.current_brand_favo_places[i].attach_url == 'attach_url') {
            this.current_brand_favo_places.splice(i, 1);
            this.storage.set(this.current_brand, this.current_brand_favo_places);
        }
        else {
            var imageDataName = this.current_brand_favo_places[i].attach_url.replace(/^.*[\\\/]/, '');
            var path = this.current_brand_favo_places[i].attach_url.replace(/[^\/]*$/, '');
            this.file.removeFile(path, imageDataName).then(function () {
                _this.current_brand_favo_places.splice(i, 1);
                _this.storage.set(_this.current_brand, _this.current_brand_favo_places);
            });
        }
    };
    FavoListPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-favo-list',
            templateUrl: 'favo-list.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, Storage, Camera, File])
    ], FavoListPage);
    return FavoListPage;
}());
export { FavoListPage };
//# sourceMappingURL=favo-list.js.map