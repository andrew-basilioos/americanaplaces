var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoListPage } from './favo-list';
import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
var FavoListPageModule = /** @class */ (function () {
    function FavoListPageModule() {
    }
    FavoListPageModule = __decorate([
        NgModule({
            declarations: [
                FavoListPage,
            ],
            imports: [
                IonicPageModule.forChild(FavoListPage),
                IonicStorageModule.forRoot()
            ],
            providers: [
                Camera,
                File
            ]
        })
    ], FavoListPageModule);
    return FavoListPageModule;
}());
export { FavoListPageModule };
//# sourceMappingURL=favo-list.module.js.map