import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoListPage } from './favo-list';

import { IonicStorageModule } from '@ionic/storage';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    FavoListPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoListPage),
    IonicStorageModule.forRoot()
  ],
  providers:[
    Camera,
    File
  ]
})
export class FavoListPageModule {}
