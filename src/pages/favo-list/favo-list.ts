import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { MapLocationPage } from '../map-location/map-location';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';

declare var cordova:any;

@IonicPage()
@Component({
  selector: 'page-favo-list',
  templateUrl: 'favo-list.html',
})
export class FavoListPage {
  current_brand;
  current_brand_favo_places = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage:Storage, private myCam: Camera, private file: File) {
  }

  ionViewWillEnter(){
    this.current_brand_favo_places = [];
    this.current_brand = this.navParams.data;
    this.storage.get(this.current_brand).then((value)=>{
      this.current_brand_favo_places = value;
    });
  }

  checkAttachedUrl(i){
    if(this.current_brand_favo_places[i].attach_url == 'attach_url'){
      return false;
    }
    return true; 
  }

  goToMap(place){
    this.navCtrl.push(MapLocationPage,{"current_place":place,"brand_name":null});
  }

  openCamera(i) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.myCam.DestinationType.FILE_URI,
      encodingType: this.myCam.EncodingType.JPEG,
      mediaType: this.myCam.MediaType.PICTURE
    }

    this.myCam.getPicture(options).then((imageData) => {
      let imageDataName = imageData.replace(/^.*[\\\/]/,'');
      let path = imageData.replace(/[^\/]*$/,'');
      this.file.moveFile(path,imageDataName,cordova.file.dataDirectory,imageDataName)
      .then((data)=>{
        this.current_brand_favo_places[i].attach_url = data.nativeURL;
        this.storage.set(this.current_brand,this.current_brand_favo_places);
      });
    }, (err) => {
      console.log(err);
    });
  }

  deleteThisPlace(i){
    if(this.current_brand_favo_places[i].attach_url == 'attach_url'){
      this.current_brand_favo_places.splice(i,1);
      this.storage.set(this.current_brand,this.current_brand_favo_places);
    }else {
      let imageDataName = this.current_brand_favo_places[i].attach_url.replace(/^.*[\\\/]/,'');
      let path = this.current_brand_favo_places[i].attach_url.replace(/[^\/]*$/,'');
      this.file.removeFile(path, imageDataName).then(()=>{
        this.current_brand_favo_places.splice(i,1);
        this.storage.set(this.current_brand,this.current_brand_favo_places);
      });
    }
  }

}
