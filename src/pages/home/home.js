var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AppService } from '../../services/app.service';
import { TranslateService } from '@ngx-translate/core';
import { BrandPage } from '../brand/brand';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, appService, translate) {
        this.navCtrl = navCtrl;
        this.appService = appService;
        translate.setDefaultLang(this.appService.lang);
        translate.use(this.appService.lang);
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.appService.getBrands()
            .subscribe(function (response) {
            _this.brands = response;
        });
    };
    HomePage.prototype.check_this_brand = function (brand) {
        this.navCtrl.push(BrandPage, brand);
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [NavController, AppService, TranslateService])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
//# sourceMappingURL=home.js.map