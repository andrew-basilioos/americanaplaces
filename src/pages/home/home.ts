import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

import { AppService } from '../../services/app.service';
import {TranslateService} from '@ngx-translate/core';

import { BrandPage } from '../brand/brand';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  brands;

  constructor(public navCtrl: NavController, private appService:AppService, translate: TranslateService,private loadingCtrl:LoadingController) {
    translate.setDefaultLang(this.appService.lang);
    translate.use(this.appService.lang);
  }

  ionViewWillEnter(){
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present();
    this.appService.getBrands()
    .subscribe((response)=>{
      loading.dismiss();
      this.brands = response;
    });
  }

  check_this_brand(brand){
    this.navCtrl.push(BrandPage,brand);
  }

}