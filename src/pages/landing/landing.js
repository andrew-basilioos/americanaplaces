var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Platform } from 'ionic-angular';
import { AppService } from '../../services/app.service';
import { HomePage } from '../home/home';
import { FavoListPage } from '../favo-list/favo-list';
import { Storage } from '@ionic/storage';
var LandingPage = /** @class */ (function () {
    function LandingPage(navCtrl, navParams, appService, platform, menuCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.appService = appService;
        this.platform = platform;
        this.menuCtrl = menuCtrl;
        this.storage = storage;
        this.favoritePlaces = [];
        this.test_img = '';
    }
    LandingPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.platform.setDir('ltr', true);
        var logo_img = document.querySelector('.logo_animated_img');
        logo_img.classList.add('logo_animated_img_animate');
        this.favoritePlaces = [];
        this.storage.forEach(function (value, key, iterationNumber) {
            if (value.length) {
                _this.favoritePlaces.push(key);
            }
        });
    };
    LandingPage.prototype.setLangAndDire = function (langAndDire) {
        this.appService.setAppLang(langAndDire.lang);
        this.appService.setAppDirection(langAndDire.dire);
        this.navCtrl.push(HomePage);
    };
    LandingPage.prototype.menu_open = function () {
        this.menuCtrl.open();
    };
    LandingPage.prototype.listingFavo = function (brand) {
        this.navCtrl.push(FavoListPage, brand);
        this.menuCtrl.close();
    };
    LandingPage.prototype.deleteTheBrand = function (i) {
        var _this = this;
        this.storage.remove(this.favoritePlaces[i]).then(function () {
            _this.favoritePlaces = [];
            _this.storage.forEach(function (value, key, iterationNumber) {
                _this.favoritePlaces.push(key);
            });
        });
    };
    LandingPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-landing',
            templateUrl: 'landing.html'
        }),
        __metadata("design:paramtypes", [NavController, NavParams, AppService, Platform, MenuController, Storage])
    ], LandingPage);
    return LandingPage;
}());
export { LandingPage };
//# sourceMappingURL=landing.js.map