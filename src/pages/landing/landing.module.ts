import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandingPage } from './landing';

import { IonicStorageModule } from '@ionic/storage';
 
@NgModule({
  declarations: [
    LandingPage
  ],
  imports: [
    IonicPageModule.forChild(LandingPage),
    IonicStorageModule.forRoot(),
  ]
})
export class LandingPageModule {}
