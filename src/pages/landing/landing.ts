import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { Platform } from 'ionic-angular';

import { AppService } from '../../services/app.service';

import { HomePage } from '../home/home';
import { FavoListPage } from '../favo-list/favo-list';

import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})
export class LandingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private appService: AppService, private platform: Platform, private menuCtrl: MenuController, private storage: Storage) {
  }

  favoritePlaces = [];
  test_img = '';

  ionViewWillEnter() {
    this.platform.setDir('ltr', true);
    let logo_animated_img_container = document.querySelector('.logo_animated_img_container');
    logo_animated_img_container.classList.add('logo_animated_img_animate');
    logo_animated_img_container.addEventListener('transitionend',()=>{
      logo_animated_img_container.classList.add('logo_animated_img_animate_two');
    })

    this.favoritePlaces = [];
    this.storage.forEach((value, key, iterationNumber) => {
      if(value.length){
        this.favoritePlaces.push(key);
      }else if(!value.length){
        this.storage.remove(key);
      }
    })
  }

  setLangAndDire(langAndDire) {
    this.appService.setAppLang(langAndDire.lang);
    this.appService.setAppDirection(langAndDire.dire);
    this.navCtrl.push(HomePage);
  }

  menu_open() {
    this.menuCtrl.open();
  }

  listingFavo(brand) {
    this.navCtrl.push(FavoListPage, brand);
    this.menuCtrl.close();
  }
}