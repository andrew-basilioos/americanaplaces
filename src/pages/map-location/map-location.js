var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
var MapLocationPage = /** @class */ (function () {
    function MapLocationPage(navCtrl, navParams, translate, toastCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.translate = translate;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
    }
    MapLocationPage.prototype.ngOnInit = function () {
        this.haveBrand = this.navParams.data.brand_name;
        this.declareMap();
    };
    MapLocationPage.prototype.declareMap = function () {
        var platform = new H.service.Platform({
            'app_id': 'riC3Q9QmywcbVrRIR89p',
            'app_code': 'uHGTy2hPpwJlGop3YvWV7w'
        });
        var defaultLayers = platform.createDefaultLayers();
        var map = new H.Map(document.getElementById('map'), defaultLayers.normal.map, {
            zoom: 16,
            center: { lat: this.navParams.data.current_place.latitude, lng: this.navParams.data.current_place.longitude }
        });
        var mapEvents = new H.mapevents.MapEvents(map);
        new H.mapevents.Behavior(mapEvents);
        var imgElement = document.createElement('img');
        imgElement.setAttribute('src', this.navParams.data.current_place.brand_pin_logo);
        imgElement.setAttribute('class', 'jump');
        var icon = new H.map.DomIcon(imgElement);
        this.coords = { lat: this.navParams.data.current_place.latitude, lng: this.navParams.data.current_place.longitude };
        var marker = new H.map.DomMarker(this.coords, { icon: icon });
        map.addObject(marker);
    };
    MapLocationPage.prototype.saveAsFavorite = function () {
        var _this = this;
        var thisbrandlocations = [];
        this.storage.get(this.navParams.data.brand_name).then(function (val) {
            if (val == null) {
                thisbrandlocations = [];
                _this.navParams.data.current_place.attach_url = 'attach_url';
                _this.navParams.data.current_place.attach_imageData = 'attach_imageData';
                thisbrandlocations.push(_this.navParams.data.current_place);
                _this.storage.set(_this.navParams.data.brand_name, thisbrandlocations);
            }
            else if (val.length) {
                thisbrandlocations = val;
                var el = thisbrandlocations.filter(function (el) {
                    return el.loc_id === _this.navParams.data.current_place.loc_id;
                });
                if (el.length) {
                    _this.toastDoesExist = _this.toastCtrl.create({
                        message: 'already added to favorite list !!!',
                        duration: 3000,
                        position: 'top'
                    });
                    _this.toastDoesExist.present();
                }
                else {
                    _this.navParams.data.current_place.attach_url = 'attach_url';
                    _this.navParams.data.current_place.attach_imageData = 'attach_imageData';
                    thisbrandlocations.push(_this.navParams.data.current_place);
                    _this.storage.set(_this.navParams.data.brand_name, thisbrandlocations);
                    _this.toast = _this.toastCtrl.create({
                        message: 'place was saved !!!',
                        duration: 3000,
                        position: 'top'
                    });
                    _this.toast.present();
                }
            }
        });
    };
    MapLocationPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-map-location',
            templateUrl: 'map-location.html',
        }),
        __metadata("design:paramtypes", [NavController, NavParams, TranslateService, ToastController, Storage])
    ], MapLocationPage);
    return MapLocationPage;
}());
export { MapLocationPage };
//# sourceMappingURL=map-location.js.map