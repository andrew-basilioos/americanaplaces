import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { ToastController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

declare var H;

@IonicPage()
@Component({
  selector: 'page-map-location',
  templateUrl: 'map-location.html',
})
export class MapLocationPage implements OnInit {
  coords;
  brand_logo;
  toast;
  toastDoesExist;
  haveBrand;
  place_was_saved;
  already_added;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, private toastCtrl: ToastController, private storage: Storage) { }

  ngOnInit() {
    this.translate.get("add_notifications").subscribe((res)=>{
      this.place_was_saved = res.place_was_saved;
      this.already_added = res.already_added;
    });
    this.haveBrand = this.navParams.data.brand_name;
    this.declareMap();
  }

  declareMap() {
    var platform = new H.service.Platform({
      'app_id': 'riC3Q9QmywcbVrRIR89p',
      'app_code': 'uHGTy2hPpwJlGop3YvWV7w'
    });

    var defaultLayers = platform.createDefaultLayers();

    var map = new H.Map(
      document.getElementById('map'),
      defaultLayers.normal.map,
      {
        zoom: 16,
        center: { lat: this.navParams.data.current_place.latitude, lng: this.navParams.data.current_place.longitude }
      });

    var mapEvents = new H.mapevents.MapEvents(map);
    new H.mapevents.Behavior(mapEvents);

    var imgElement = document.createElement('img');
    imgElement.setAttribute('src', this.navParams.data.current_place.brand_pin_logo);
    imgElement.setAttribute('class', 'jump');

    var icon = new H.map.DomIcon(imgElement);
    this.coords = { lat: this.navParams.data.current_place.latitude, lng: this.navParams.data.current_place.longitude };
    var marker = new H.map.DomMarker(this.coords, { icon: icon });    

    map.addObject(marker);
  }

  saveAsFavorite() {
    let thisbrandlocations = [];
    this.storage.get(this.navParams.data.brand_name).then((val) => {
    if (val == null) {
      thisbrandlocations = [];
      this.navParams.data.current_place.attach_url = 'attach_url';
      thisbrandlocations.push(this.navParams.data.current_place);
      this.storage.set(this.navParams.data.brand_name, thisbrandlocations);
      this.toast = this.toastCtrl.create({
        message: this.place_was_saved,
        duration: 3000,
        position: 'top'
      });
      this.toast.present();
    } else if (val.length) {
        thisbrandlocations = val;
        let el = thisbrandlocations.filter((el) => {
          return el.loc_id === this.navParams.data.current_place.loc_id;
        });
        if (el.length) {
          this.toastDoesExist = this.toastCtrl.create({
            message: this.already_added,
            duration: 3000,
            position: 'top'
          });
          this.toastDoesExist.present();
        } else if(!el.length) {
          this.navParams.data.current_place.attach_url = 'attach_url';
          thisbrandlocations.push(this.navParams.data.current_place);
          this.storage.set(this.navParams.data.brand_name, thisbrandlocations);
          this.toast = this.toastCtrl.create({
            message: this.place_was_saved,
            duration: 3000,
            position: 'top'
          });
          this.toast.present();
        }
      }
    });
  }

}
