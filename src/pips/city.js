var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Pipe } from '@angular/core';
var CityName = /** @class */ (function () {
    function CityName() {
    }
    CityName.prototype.transform = function (value, names, current_country_id) {
        var current_country_obj;
        var current_city_obj;
        current_country_obj = names.filter(function (data) {
            return data.country_id == current_country_id;
        });
        current_city_obj = current_country_obj[0].cities.filter(function (data) {
            return data.city_id == value;
        });
        if (current_city_obj[0] != null) {
            return current_city_obj[0].city_name;
        }
    };
    CityName = __decorate([
        Pipe({
            name: 'city_name'
        }),
        __metadata("design:paramtypes", [])
    ], CityName);
    return CityName;
}());
export { CityName };
//# sourceMappingURL=city.js.map