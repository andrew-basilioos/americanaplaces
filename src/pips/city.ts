import { Pipe,PipeTransform } from '@angular/core';

@Pipe({
    name:'city_name'    
})
export class CityName implements PipeTransform{
    constructor(){}

    transform(value : any,names : any,current_country_id : any){
        let current_country_obj;
        let current_city_obj;
        current_country_obj = names.filter(data=>{
            return data.country_id == current_country_id;
        })
        current_city_obj = current_country_obj[0].cities.filter(data=>{
            return data.city_id == value;
        });
        if(current_city_obj[0] != null){
            return current_city_obj[0].city_name;
        }
    }
}