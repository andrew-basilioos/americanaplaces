import { Pipe,PipeTransform } from '@angular/core';

@Pipe({
    name:'country_name'    
})
export class CountryName implements PipeTransform{
    constructor(){}

    transform(value : any,names : any){
        let country_obj;
        if(names != null){
            country_obj = names.filter((data:any)=>{
                return data.country_id == value;
            });
            return country_obj[0].country_name;
        }
    }
}