var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
//AppService is to get and set global app vars
var AppService = /** @class */ (function () {
    function AppService(httpClient, platform) {
        this.httpClient = httpClient;
        this.platform = platform;
        this.base_url = 'https://americana-group.com/wp-json/webservice/v2/';
    }
    AppService.prototype.getBrands = function () {
        return this.httpClient.get(this.base_url + 'brands', {
            params: new HttpParams().set('lang', this.lang)
        });
    };
    AppService.prototype.getCountriesInfo = function () {
        return this.httpClient.get(this.base_url + 'countries', {
            params: new HttpParams().set('lang', this.lang)
        });
    };
    AppService.prototype.setAppLang = function (lang) {
        this.lang = lang;
    };
    AppService.prototype.setAppDirection = function (dire) {
        if (dire == "ltr") {
            this.platform.setDir('ltr', true);
        }
        else if (dire == "rtl") {
            this.platform.setDir('rtl', true);
        }
    };
    AppService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [HttpClient, Platform])
    ], AppService);
    return AppService;
}());
export { AppService };
//# sourceMappingURL=app.service.js.map