import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

//AppService is to get and set global app vars
@Injectable()
export class AppService {
    lang;
    base_url = 'https://americana-group.com/wp-json/webservice/v2/';

    constructor(private httpClient:HttpClient,private platform:Platform){}

    getBrands(){
        return this.httpClient.get(this.base_url+'brands',{
            params:new HttpParams().set('lang',this.lang)
        });
    }

    getCountriesInfo(){
        return this.httpClient.get(this.base_url+'countries',{
            params:new HttpParams().set('lang',this.lang)
        });
    }

    setAppLang(lang){
        this.lang = lang;
    }

    setAppDirection(dire){
        if(dire == "ltr"){
            this.platform.setDir('ltr',true);
        }else if(dire == "rtl"){
            this.platform.setDir('rtl',true);
        }
    }
}